# Natural Computing Assignment 1: Collective Migration Cellular Pott's Model

## Instructions

1. Download or fork Artistoo into `src/static/vendor/artistoo`. Make sure to follow the installation guide.
2. Install python dependencies as defined in `pyproject.toml`. Different python versions and different dependecies' versions likely work, but have not been checked.
3. Optionally change default configuration parameters in `src/static/config.js`.
4. Run from project's root directory via `python src/app.py`.
5. Uvicorn now serves a simple website on your localhost, accessible on `http://127.0.0.1:8000`. To run a simulation, it is enough to visit the website, however you can provide custom values for the number of cells and the number of obstacles via queries in the URL: `http://127.0.0.1:8000/?cells=x&obstacles=y` (change `x` and `y` to desired values).

Please note: By default, running any simulation results in automatic download of simulation data and simulation screenshots. This behaviour can be disabled in `src/static/config.js`.
