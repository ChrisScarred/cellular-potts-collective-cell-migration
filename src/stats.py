import pandas as pd
import numpy as np
from os import listdir
from os.path import isfile, join
from typing import Optional, List, Tuple, Dict
from scipy.stats import ttest_ind_from_stats

DATA_DIR = "data"

def get_files(path: str, ext: Optional[str] = None) -> List[str]:
    files = [f for f in listdir(path) if isfile(join(path, f))]
    if ext:
        if not ext.startswith("."):
            ext = "." + ext
        return [f for f in files if f.endswith(ext)]
    return files

def parse_name(fname: str) -> Tuple[int, int, int]:
    cells, obstacles = fname.split("_")
    cells = int(cells)
    obstacles = obstacles.split(".")[0]
    set = 1
    if "(" in obstacles:
        obstacles, rest = obstacles.split("(")
        set = int(rest.split(")")[0]) + 1
    obstacles = int(obstacles)
    return cells, obstacles, set

def read_csv(fpath: str) -> Tuple[np.ndarray, np.ndarray]:
    df = pd.read_csv(fpath)
    return df["t"].to_numpy(), df["velocity"].to_numpy()

def merge_files(data_dir: str, ext: Optional[str] = ".csv", result_file: str = "agg/raw_results.csv"):
    files = get_files(data_dir, ext)
    results = []
    for f in files:
        cells, obstacles, set_ = parse_name(f)
        obstacles = obstacles ** 2
        ts, velocities = read_csv(join(data_dir, f))
        for (t, v) in zip(ts, velocities):
            result = {
                "set": set_,
                "cell_count": cells,
                "obstacle_count": obstacles,
                "t": t,
                "velocity": v
            }
            results.append(result)
    df = pd.DataFrame(data=results)
    df.to_csv(join(data_dir, result_file))

def process_results(raw_results_file: str, processed_results_file: str):
    df = pd.read_csv(raw_results_file, index_col=0)
    df = df.drop(columns=["set"])
    df = df[df.velocity != 0]
    groupped = df.groupby(["t", "cell_count", "obstacle_count"])
    means = groupped.mean().reset_index()
    stds = groupped.std().reset_index()
    means["std"] = stds["velocity"]
    means["cell_count"] = means["cell_count"].apply(lambda x: int(((x*200)/(200**2))*100))
    means["obstacle_count"] = means["obstacle_count"].apply(lambda x: int(((x*100)/(200**2))*100))
    newDf = means.rename(columns={"velocity": "avg_velocity", "cell_count": "cell_density", "obstacle_count": "obstacle_density"})
    newDf.to_csv(processed_results_file)

def filter_df_to_timeseries(df: pd.DataFrame, obs_condition: int, cell_condition: int, times: List[int]) -> np.ndarray:
    results = []
    for t in sorted(times):
        f = df.query("obstacle_density==@obs_condition and cell_density==@cell_condition and t==@t")
        results.append([float(f["avg_velocity"].to_numpy()), float(f["std"].to_numpy())])
    return np.array(results)

def construct_timeseries(df: pd.DataFrame) -> Tuple[Dict, List[int], List[int], List[int]]:
    cell_conditions = np.unique(df["cell_density"].to_numpy())
    times = np.unique(df["t"].to_numpy())
    obs_conditions = np.unique(df["obstacle_density"].to_numpy())
    results = {}
    for obs_condition in obs_conditions:
        results[obs_condition] = {}
        for cell_condition in cell_conditions:
            results[obs_condition][cell_condition] = filter_df_to_timeseries(df, obs_condition, cell_condition, times)
    return results, obs_conditions, cell_conditions, times

def significance(file: str, th: float = 0.05):
    df = pd.read_csv(file)
    results = []
    data, obs_conditions, cell_conditions, times = construct_timeseries(df)
    for cells in cell_conditions:
        control = data[0][cells]
        for obs in obs_conditions:
            if obs > 0:
                condition = data[obs][cells]
                ps = []
                for i in range(condition.shape[0]):
                    ps.append(ttest_ind_from_stats(condition[i][0], condition[i][1], 3, control[i][0], control[i][1], 3)[1])
                result = {
                    "cell_density": cells,
                    "obstacle_density": obs
                }
                for i, p in enumerate(ps):
                    result[f"value at t={times[i]}"] = condition[i][0]
                    result[f"std at t={times[i]}"] = condition[i][1]
                    result[f"p at t={times[i]}"] = p
                    result[f"significance at t={times[i]}"] = p < th
                results.append(result)
    new_df = pd.DataFrame(data=results)
    new_df.to_csv("data/agg/significance.csv")
    """
    means = df["avg_velocity"].to_numpy()
    stds = df["std"].to_numpy()
    simulated_population = []
    for i in range(means.size):
        simulated_population.extend(list(np.random.normal(loc=means[i], scale=stds[i], size=50)))
    sim_pop_mean = np.mean(np.array(simulated_population))
    sim_pop_std = np.std(np.array(simulated_population))
    n = len(simulated_population)
    significant = []
    insignificant = []
    for i in range(means.size):
        _, p = ttest_ind_from_stats(means[i], stds[i], 3, sim_pop_mean, sim_pop_std, n)
        res = df.iloc[[i]].drop(columns=["Unnamed: 0"]).to_dict(orient="records")
        print(res)
        if p < th:
            significant.extend(res)
        else:
            insignificant.extend(res)
    significant_df = pd.DataFrame(data=significant)
    insignificant_df = pd.DataFrame(data=insignificant)
    print(significant_df)
    print(insignificant_df)
    """


if __name__=="__main__":
#    merge_files(DATA_DIR)
#    process_results("data/agg/raw_results.csv", "data/agg/processed_results.csv")
    significance("data/agg/processed_results.csv")
