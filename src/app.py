from fastapi import FastAPI, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

app = FastAPI()
templates = Jinja2Templates(directory="src/template")
app.mount("/static", StaticFiles(directory="src/static"), name="static")

@app.get("/", response_class=HTMLResponse)
async def run_app(request: Request, cells: int = 10, obstacles: int = 0):
    # /?cells=x&obstacles=y
    return templates.TemplateResponse(
        request=request, name="collective_migration_cpm.html", context={"cells": cells, "obstacles": obstacles}
    )


if __name__=="__main__":
    import uvicorn
    uvicorn.run("app:app")
