function newArray(val, dim1, dim2) {
    if (typeof(dim2) == "int") {
        return Array(dim1).fill(val).map((_) => Array(dim2).fill(val));
    }
    return Array(dim1).fill(val);
}

function sumOfArray(arr) {
    return arr.reduce((x, a) => x + a, 0);
}

function filterOnKind(cellMetric, cellKind, model) {
    let values = [];
    for (const [key, value] of Object.entries(cellMetric)) {
        if (model.cellKind(key) == cellKind) {
            values.push(value);
        }
    }
    return values;
}

function getStats(model, stat) {
    return model.getStat(stat);
}

function keyJoin(dict, joiner) {
    return Object.keys(dict).join(joiner)
}

function keyValProcess(dict, processor) {
    let res = {};
    for (const [key, value] of Object.entries(dict)) {
        res[key] = processor(value);
    }
    return res;
}

function getRandomPosition(canvas_size) {
    /**
     * Get a random position on a square canvas
     */
    return [
      Math.round(Math.random() * (canvas_size - 1)),
      Math.round(Math.random() * (canvas_size - 1))
    ]
  }

function getFilteredStat(model, stat, cellKind) {
    return filterOnKind(getStats(model, stat), cellKind, model);
}

class MigratingCPM {
    /**
     * Cellular Potts model of collective migration.
     *
     * Attributes:
     * TODO: fill
     */
    constructor (params) {
      this.params = updateWith(params, defaultConfig);
      this.initSim();
      this.initMetrics();
      this.initFeatures();
    }

    initSim() {
        this.sim = new CPM.Simulation(getArtistooConfig(this.params), {
            initializeGrid: this.initializeGrid(),
            // initializeGrid is a closure (return a function upon being called)
        });
        this.model = this.sim.C;
    }
  
    initFeatures () {
      if (this.params.showMeter) {
        this.meter = new FPSMeter(this.params.meterConfig);
      }
      if (this.params.dataDownload) {
        this.records = "t,velocity" + '\n';
        this.dataElem = document.getElementById(this.params.dataId);
        this.dataElem.onclick = (_) => {
          var a = document.createElement("a");
          document.body.appendChild(a);
          a.style = "display: none";
          var data = new Blob([this.records], { type: 'text/csv' });
          var url = window.URL.createObjectURL(data);
          a.href = url;
          a.download = this.params.nCells + "_" + this.params.nObstacles + ".csv";
          a.click();
          window.URL.revokeObjectURL(url);
        }
      }
    }
  
    initMetrics () {
      this.metrics = {
        centroids: newArray(0, this.params.nCells, this.params.nDims),
        displacement: newArray(0, this.params.dtVelocity, this.params.nCells),
        velocity: newArray(0, this.params.nCells),
        instaVelocity: 0,
        avgVelocity: 0
      };
      if (this.params.showVelocity) {
        this.instaVelocityElement = document.getElementById(this.params.velocityId);
      }
    }

    initializeGrid () {
      /**
       * Randomly put the migrating cells on the canvas and the place the obstacles on a regular grid.
       */
      // in the var declarations, this refers to MigratingCPM object
      var nCells = this.params.nCells;
      var canvasSize = this.params.canvasSize;
      var nObstacles = this.params.nObstacles;

      // in the return statement, this refers to the Simulation object
      return function () {
        if (!this.helpClasses.gm) {
          this.addGridManipulator();
        }

        if (nObstacles > 0) {
          let step = Math.floor(canvasSize / nObstacles)
          for (let i = 0; i < this.C.extents[0]; i += step) {
            for (let j = 0; j < this.C.extents[1]; j += step) {
              this.gm.seedCellAt(2, [i, j]);
            }
          }
        }

        if (nCells > 0) {
          for (let i = 0; i < nCells; i += 1) {
            this.gm.seedCellAt(1, getRandomPosition(canvasSize));
          }
        }
      }
    }

    updateCentroidsAndDisplacement() {
        let newCentroids = getFilteredStat(this.model, CPM.CentroidsWithTorusCorrection, 1);
        let displacement = [];
        for (let i = 0; i < newCentroids.length; i++) {
            let v = 0;
            for (let j = 0; j < this.params.nDims; j++) {
                let diff = Math.abs(newCentroids[i][j] - this.metrics.centroids[i][j]);
                v += Math.pow(Math.min(diff, this.params.canvasSize - diff), 2);
            }
            displacement.push(Math.sqrt(v));
        }
        this.metrics.centroids = newCentroids;
        this.metrics.displacement = this.metrics.displacement.slice(1);
        this.metrics.displacement[this.metrics.displacement.length] = displacement;
    }

    updateVelocityPerCell() {
      let velocities = [];
      for (let i = 0; i < this.params.nCells; i++) {
          let velocitySum = 0;
          for (let j = 0; j < this.metrics.displacement.length; j++) {
            if (this.metrics.displacement[j][i]) {
                velocitySum += this.metrics.displacement[j][i];
            }
          }
          velocities.push(velocitySum / this.params.dtVelocity);
        }
      this.metrics.velocity = velocities;
    }

    updateInstaVelocity() {
        let sum = sumOfArray(this.metrics.velocity);
        let newValue = (sum / this.metrics.velocity.length) * 1000;
        this.metrics.instaVelocity = newValue;
        return newValue;
    }

    updateDisplayedMetrics() {
      let t = this.sim.time;
      let newValue = 0;
      if (t % this.params.updateStep === 1) {
            let sum = sumOfArray(this.metrics.velocity);
            newValue = (sum / this.metrics.velocity.length) * 1000;
            this.metrics.instaVelocity = newValue;
            this.instaVelocityElement.innerHTML = Math.round(newValue);
        }
      if (t % this.params.runtimeBreakpoint === 1 & this.params.dataDownload) {
        this.records += (t-1) + "," + newValue + "\n";
        this.capture(t);
      }
      if (t+1 == this.params.runtime) {
        this.dataElem.click();
      }
    }

    capture(t) {
      if (this.params.capture) {
        let canvas = document.querySelector('canvas');
        let image = new Image();
        image.src = canvas.toDataURL('image/png');
        let link = document.createElement('a');
        link.href = image.src;
        link.download = `screenshot_with_${this.params.nCells}_cells_and_${this.params.nObstacles}_obstacles_at_T_${t-1}.png`;
        link.click();
      }
    }

    run () {
      /**
       * Perform the simulation.
       */
      this.sim.step();
      if (this.meter) {
        this.meter.tick();
      }
      let self = this;
      if (
        (this.sim.conf['RUNTIME_BROWSER'] == 'Inf') |
        (this.sim.time + 1 < this.sim.conf['RUNTIME_BROWSER'])
      ) {
        requestAnimationFrame(self.run.bind(self));
      }
      this.updateCentroidsAndDisplacement();
      this.updateVelocityPerCell();
      this.updateDisplayedMetrics();
    }
  }

function migratingCPM(params) {
  /**
   * Initialise the collective migration cellular Potts model and run its simulation.
   */
  let mCPM = new MigratingCPM(params);
  mCPM.run();
  return;
}
