defaultConfig = {
    // show activity colours for the cells and for the obstacle
    actColours: [true, false],
    // importance of the activity term of the cells
    actLambda: 200,
    // maximum actvity of cells remembered
    actMax: 80,
    // mean-calculating strategy for activity
    actMean: "geometric",
    // adhesion of the cell to the background and background to the cell
    adhesion: 20,
    // burn-in period in Monte Carlo steps (MCS)
    burnin: 500,
    canvasColour: "eaecef",
    // size of one side of the square canvas
    canvasSize: 200,
    // whether or not to make screenshots of the simulation at every runtimeBreakpoint
    capture: true,
    // colour of the cells and the obstacles
    cellColours: ["000000", "6d706e"],
    // whether to use a custom parent element for the canvas tag
    customParent: true,
    // whether to enable data download
    dataDownload: true,
    // the id of the button that is to be pressed for downloading data
    dataId: "data",
    // the number of latest steps to consider when calculating the instantaneous velocity
    dtVelocity: 10,
    // the FPS meter config
    meterConfig: { left: 'auto', right: '5px' },
    // the number of cells to spawn
    nCells: 10,
    // the number of dimensions
    nDims: 2,
    // the number of obstacles to spawn in each row/column
    nObstacles: 0,
    // the scale of obstacles as compared to the cells
    // obstacle volume and obstacle perimeter are calculated
    // as cell volume / obsScaledown and cell perimeter / obsScaledown
    obsScaledown: 2,
    // the id of the parent element of the canvas tag
    parentId: "canvas",
    // the importance of the perimeter term of the cells
    perimeterLambda: 2,
    // the perimeter of the cells
    perimeterVal: 180,
    // maximum runtime in MCS
    runtime: 2500,
    // points in time as MCS when to record data
    runtimeBreakpoint: 300,
    seed: 1,
    showBorders: [false, false],
    // whether to show the FPS meter
    showMeter: true,
    // temperature of the system
    temperature: 20,
    // whether a direction is toroidal
    torus: [true, true],
    // how often to update the metrics shown in the UI in MCS
    updateStep: 50,
    // importance of the volume term of the cells
    volumeLambda: 50,
    // the volume of the cells
    volumeVal: 200,
    zoom: 2,
    showVelocity: true,
    velocityId: "insta_velo",
}

function updateWith(og, upd) {
    for (const [key, value] of Object.entries(upd)) {
        if (typeof og[key] == 'undefined') {
            og[key] = value
        }
      }
    return og
}

function getArtistooConfig(params) {
    return {
        ndim: params.nDims,
        field_size: [params.canvasSize, params.canvasSize],
        conf: {
            torus: params.torus,
            seed: params.seed,
            T: params.temperature,
            J: [
                [0, params.adhesion, 0],
                [params.adhesion, 0, 0],
                [0, 0, 0]
            ],
            LAMBDA_V: [0, params.volumeLambda, 200],
            V: [0, params.volumeVal, params.volumeVal / params.obsScaledown],
            LAMBDA_P: [0, params.perimeterLambda, 1000],
            P: [0, params.perimeterVal, params.perimeterVal / params.obsScaledown],
            LAMBDA_ACT: [0, params.actLambda, 0],
            MAX_ACT: [0, params.actMax, 0],
            ACT_MEAN: params.actMean
            },
        simsettings: {
            parentElement: params.customParent? document.getElementById(params.parentId) : document.body,
            NRCELLS: [params.nCells, params.nObstacles],
            BURNIN: params.burnin,
            RUNTIME: params.runtime,
            RUNTIME_BROWSER: params.runtime,
            CANVASCOLOR: params.canvasColour,
            CELLCOLOR: params.cellColours,
            ACTCOLOR: params.actColours,
            SHOWBORDERS: params.showBorders,
            zoom: params.zoom,
        }
    }
}
